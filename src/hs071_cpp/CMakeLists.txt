cmake_minimum_required(VERSION 3.11)
project(hs071_cpp CXX)

find_package(Ipopt REQUIRED)

add_executable(hs071_cpp hs071_main.cpp hs071_nlp.cpp hs071_nlp.hpp)

target_link_libraries(hs071_cpp Ipopt::Ipopt)

