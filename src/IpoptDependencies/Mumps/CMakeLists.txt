cmake_minimum_required(VERSION 3.11)

project(IpoptMumps Fortran C)


SET(MUMPS_ROOTDIR ${CMAKE_CURRENT_SOURCE_DIR}/MUMPS)
SET(MUMPS_SRCDIR ${MUMPS_ROOTDIR}/src)
SET(MUMPS_INCDIR ${MUMPS_ROOTDIR}/include )
SET(MUMPS_LIQDIR ${MUMPS_ROOTDIR}/libseq )
SET(MUMPS_SRC /MUMPS/src)

SET(MUMPS_C_SOURCES
    MUMPS/src/mumps_c.c
    MUMPS/src/mumps_common.c
    MUMPS/src/mumps_orderings.c
    MUMPS/src/mumps_io.c
    MUMPS/src/mumps_io_basic.c
    MUMPS/src/mumps_io_thread.c
    MUMPS/src/mumps_io_err.c
    MUMPS/src/mumps_size.c
    MUMPS/libseq/mpic.c
    MUMPS/libseq/elapse.c
    )


SET(MUMPS_FORTRAN_SOURCES
    MUMPS/src/tools_common_mod.F
    MUMPS/src/dmumps_comm_buffer.F
    MUMPS/src/dmumps_struc_def.F
    MUMPS/src/mumps_ooc_common.F
    MUMPS/src/mumps_static_mapping.F
    MUMPS/src/dmumps_ooc_buffer.F
    MUMPS/src/dmumps_load.F
    MUMPS/src/dmumps_ooc.F
    MUMPS/src/mumps_sol_es.F
    MUMPS/src/dmumps_part2.F
    MUMPS/src/dmumps_part1.F
    MUMPS/src/dmumps_part3.F
    MUMPS/src/dmumps_part4.F
    MUMPS/src/dmumps_part5.F
    MUMPS/src/dmumps_part6.F
    MUMPS/src/dmumps_part7.F
    MUMPS/src/dmumps_part8.F
    MUMPS/src/mumps_part9.F
    MUMPS/libseq/mpi.f
    )

add_library(IpoptMumps SHARED ${MUMPS_FORTRAN_SOURCES} ${MUMPS_C_SOURCES})


add_library(IpoptMumps::IpoptMumps ALIAS IpoptMumps)


target_include_directories(IpoptMumps 
    PUBLIC $<BUILD_INTERFACE:${MUMPS_LIQDIR}> $<BUILD_INTERFACE:${MUMPS_INCDIR}>
    $<INSTALL_INTERFACE:include/Mumps/libseq>  $<INSTALL_INTERFACE:include/Mumps/include> )

set_target_properties(IpoptMumps PROPERTIES POSITION_INDEPENDENT_CODE ON)
if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
    #message("CMAKE_Fortran_COMPILER_VERSION = ${CMAKE_Fortran_COMPILER_VERSION}")
    if(CMAKE_Fortran_COMPILER_VERSION VERSION_GREATER_EQUAL 10)
        set_source_files_properties(${MUMPS_FORTRAN_SOURCES} PROPERTIES COMPILE_FLAGS "-fallow-argument-mismatch")
    endif()
    target_compile_options(IpoptMumps PRIVATE "-w") # switch of warnings
endif()
if(MINGW)
target_compile_options(IpoptMumps PRIVATE "-fvisibility=hidden")
endif()

set_property(SOURCE ${MUMPS_FORTRAN_SOURCES} PROPERTY  COMPILE_DEFINITIONS metis)

if(MINGW)
    set(DLLEXPORT_SPEC "MUMPS_CALL=__declspec(dllexport)")
endif()
set_property(SOURCE ${MUMPS_C_SOURCES} PROPERTY COMPILE_DEFINITIONS
     MUMPS_BUILD Add_ WITHOUT_PTHREAD=1 ALLOW_NON_INIT MUMPS_ARITH=MUMPS_ARITH_d ${DLLEXPORT_SPEC})

     
target_link_libraries(IpoptMumps PRIVATE IpoptMetis::IpoptMetis PUBLIC IpoptLapack::IpoptLapack PUBLIC IpoptBlas::IpoptBlas)




### installation related stuff

install(TARGETS IpoptMumps EXPORT IpoptMumpsTargets
    LIBRARY DESTINATION lib COMPONENT IpoptMumps
    ARCHIVE DESTINATION lib COMPONENT IpoptMumps
    RUNTIME DESTINATION bin COMPONENT IpoptMumps
    INCLUDES DESTINATION include)

install(EXPORT IpoptMumpsTargets
    FILE IpoptMumpsTargets.cmake
    NAMESPACE IpoptMumps::
    DESTINATION lib/cmake/IpoptMumps
    COMPONENT IpoptMumps)

install(FILES "IpoptMumpsConfig.cmake"
    DESTINATION lib/cmake/IpoptMumps
    COMPONENT IpoptMumps)

install(DIRECTORY ${MUMPS_LIQDIR}
    DESTINATION "include/Mumps"
    COMPONENT IpoptMumps
    FILES_MATCHING PATTERN "*.h"
    )

install(DIRECTORY ${MUMPS_INCDIR}
    DESTINATION "include/Mumps"
    COMPONENT IpoptMumps
    FILES_MATCHING PATTERN "*.h"
    )


##### CPack stuff ###
include(CPack)
include(CPackIFW)
set(CPACK_PACKAGE_NAME "IpoptMumps")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Linear Algebra Package")
SET(CPACK_PACKAGE_VENDOR "raha01")
SET(CPACK_PACKAGE_VERSION_MAJOR "1")
SET(CPACK_PACKAGE_VERSION_MINOR "0")
SET(CPACK_PACKAGE_VERSION_PATCH "0")
SET(CPACK_IFW_VERBOSE ON)
cpack_add_component(IpoptMumps DISPLAY_NAME "IpoptMumps" DESCRIPITON "Linear Algebra Package")
cpack_ifw_configure_component(IpoptMumps
    VERSION "1.0.0" # Version of component
    DEFAULT TRUE)
