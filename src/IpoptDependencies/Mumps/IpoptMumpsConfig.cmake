include(CMakeFindDependencyMacro)
find_dependency(IpoptBlas)
find_dependency(IpoptLapack)
include("${CMAKE_CURRENT_LIST_DIR}/IpoptMumpsTargets.cmake")