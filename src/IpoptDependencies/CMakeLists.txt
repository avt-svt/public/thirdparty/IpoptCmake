cmake_minimum_required(VERSION 3.11)
project(IpoptDependencies)


if(MINGW)
  SET(CMAKE_GNUtoMS ON CACHE BOOL "Convert GNU import libraries to MS format (requires Visual Studio)" FORCE)
  SET(CMAKE_CXX_VISIBILITY_PRESET hidden)
  SET(CMAKE_C_VISIBILITY_PRESET hidden)
  SET(CMAKE_Fortran_VISIBILITY_PRESET hidden)
  SET (CMAKE_VISIBILITY_INLINES_HIDDEN ON)
  SET(CMAKE_POSITION_INDEPENDENT_CODE  ON)
  SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
  SET(BUILD_SHARED_LIBRARIES OFF)
  SET(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++ -static -static-libgfortran")
  SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "-s -static-libgcc -static-libstdc++ -static -static-libgfortran")
  SET(CMAKE_SHARED_LINKER_FLAGS "-static-libgcc -static-libstdc++ -static -static-libgfortran")
  SET(CMAKE_SHARED_LINKER_FLAGS_RELEASE "-s -static-libgcc -static-libstdc++ -static -static-libgfortran")
  SET(CMAKE_IMPORT_LIBRARY_SUFFIX .lib)
  SET(CMAKE_IMPORT_LIBRARY_PREFIX "")
  SET(CMAKE_SHARED_LIBRARY_PREFIX "")
endif()


add_subdirectory(Blas)
add_subdirectory(Lapack)
add_subdirectory(Metis)
add_subdirectory(Mumps)

if(UNIX)
    set_target_properties(IpoptBlas IpoptLapack PROPERTIES COMPILE_FLAGS "-Wno-attributes")
    set_target_properties(IpoptMetis PROPERTIES COMPILE_FLAGS "-Wno-shift-op-parentheses -Wno-implicit-function-declaration")
endif()


