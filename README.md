# IpoptCmake

This is another port of Ipopt to the Cmake build system for cross platform development (tested on Linux, Windows). For Windows, in the main master branch. Fortran libraries are pre-compiled with mingw-gfortran. In the 3.14.12 branch, there are no pre-compiled libraries.
