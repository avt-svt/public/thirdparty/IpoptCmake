#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "IpoptMumps::IpoptMumps" for configuration "Release"
set_property(TARGET IpoptMumps::IpoptMumps APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(IpoptMumps::IpoptMumps PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/IpoptMumps.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/IpoptMumps.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS IpoptMumps::IpoptMumps )
list(APPEND _IMPORT_CHECK_FILES_FOR_IpoptMumps::IpoptMumps "${_IMPORT_PREFIX}/lib/IpoptMumps.lib" "${_IMPORT_PREFIX}/bin/IpoptMumps.dll" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
